package com.task.management.enumeration;

public enum Status {
    CREATED, IN_PROGRESS, DONE, ABANDONED
}
