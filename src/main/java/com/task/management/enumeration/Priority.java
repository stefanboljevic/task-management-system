package com.task.management.enumeration;

public enum Priority {
    LOW, MEDIUM, HIGH
}
