package com.task.management.controller;

import com.task.management.exception.InvalidTaskIdException;
import com.task.management.exception.NotAllowedPropertyConfigurationException;
import com.task.management.exception.PropertyNotNullableException;
import com.task.management.exception.TaskNotFoundException;
import com.task.management.model.dto.TaskDTO;
import com.task.management.service.implementation.TaskHandlingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    @Autowired
    private TaskHandlingService taskHandlingService;

    @GetMapping("/all")
    public List<TaskDTO> getAlLTasks() {
        return taskHandlingService.getAllTasks();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getTask(@PathVariable String id) {
        try {
            return new ResponseEntity<>(taskHandlingService.getTask(id), HttpStatus.OK);
        } catch (InvalidTaskIdException e) {
            return new ResponseEntity<>("Invalid task ID", HttpStatus.BAD_REQUEST);
        } catch (TaskNotFoundException e) {
            return new ResponseEntity<>("Task not found for given id - " + id, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createTask(@RequestBody TaskDTO taskDTO) {
        try {
            taskHandlingService.createTask(taskDTO);
            return new ResponseEntity<>("Successfully created task!", HttpStatus.CREATED);
        } catch (NotAllowedPropertyConfigurationException e) {
            return new ResponseEntity<>("Some fields are not allowed to be preconfigured!", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>("Error while trying to create task, please check log.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update")
    public ResponseEntity<Object> updateTask(@RequestBody TaskDTO taskDTO) {
        try {
            TaskDTO savedTaskDTO = taskHandlingService.updateTask(taskDTO);
            return new ResponseEntity<>(savedTaskDTO, HttpStatus.ACCEPTED);
        } catch (InvalidTaskIdException e) {
            return new ResponseEntity<>("Invalid task ID!", HttpStatus.BAD_REQUEST);
        } catch (PropertyNotNullableException e) {
            return new ResponseEntity<>("Trying to change read-only fields!", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>("Error happened while updating the task. Please check log", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{taskId}")
    public ResponseEntity<Object> deleteTask(@PathVariable String taskId) {
        try {
            taskHandlingService.deleteTask(taskId);
            return new ResponseEntity<>("Successfully deleted task!", HttpStatus.ACCEPTED);
        } catch (InvalidTaskIdException e) {
            return new ResponseEntity<>("Invalid task ID!", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>("Error while deleting the task. Please check log.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
