package com.task.management.model.dto;

import com.task.management.enumeration.Priority;
import com.task.management.enumeration.Status;
import com.task.management.model.entity.Task;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public class TaskDTO {

    public static final String DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy hh:mm:ss";
    private String id;
    private String title;
    private String description;
    private String priority;
    private String status;
    private String createdAt;

    private String updatedAt;
    private String resolvedAt;
    private String dueDate;

    private TaskDTO(TaskDTOBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.description = builder.description;
        this.priority = builder.priority;
        this.status = builder.status;
        this.createdAt = builder.createdAt;
        this.updatedAt = builder.updatedAt;
        this.resolvedAt = builder.resolvedAt;
        this.dueDate = builder.dueDate;
    }

    public Task toTask() {
        return new Task.TaskBuilder(id, title)
                .withPriority(priority)
                .withStatus(status)
                .createdAt(createdAt)
                .updatedAt(updatedAt)
                .resolvedAt(resolvedAt)
                .withDueDate(dueDate)
                .withDescription(description)
                .build();
    }

    @Override
    public String toString() {
        return "TaskDTO{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", priority='" + priority + '\'' +
                ", status='" + status + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", resolvedAt='" + resolvedAt + '\'' +
                ", dueDate='" + dueDate + '\'' +
                '}';
    }

    public static class TaskDTOBuilder {
        private String id;
        private final String title;
        private String description;
        private String priority;
        private String status;
        private String createdAt;

        private String updatedAt;
        private String resolvedAt;
        private String dueDate;

        public TaskDTOBuilder(String taskTitle) {
            this.title = taskTitle;
        }

        public TaskDTOBuilder(Integer taskId, String taskTitle, String taskDescription) {
            this.id = String.valueOf(taskId);
            this.title = taskTitle;
            this.description = taskDescription;
        }

        public TaskDTOBuilder withPriority(Priority priority) {
            if (priority != null) {
                this.priority = priority.toString();
            }
            return this;
        }

        public TaskDTOBuilder withStatus(Status status) {
            if (status != null) {
                this.status = status.toString();
            }
            return this;
        }

        public TaskDTOBuilder createdAt(Date createdAt) {
            if (createdAt != null) {
                this.createdAt = createdAt.toString();
            }
            return this;
        }

        public TaskDTOBuilder updatedAt(Date updatedAt) {
            if (updatedAt != null) {
                this.updatedAt = updatedAt.toString();
            }
            return this;
        }

        public TaskDTOBuilder resolvedAt(Date resolvedAt) {
            if (resolvedAt != null) {
                this.resolvedAt = resolvedAt.toString();
            }
            return this;
        }

        public TaskDTOBuilder withDueDate(Date dueDate) {
            if (dueDate != null) {
                DateFormat format = new SimpleDateFormat(DD_MM_YYYY_HH_MM_SS);
                this.dueDate = format.format(dueDate);
            }
            return this;
        }

        public TaskDTOBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public TaskDTO build() {
            return new TaskDTO(this);
        }
    }
}
