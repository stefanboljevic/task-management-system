package com.task.management.model.entity;

import com.task.management.enumeration.Priority;
import com.task.management.enumeration.Status;
import com.task.management.model.dto.TaskDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    private Date updatedAt;

    private Date dueDate;

    private Date resolvedAt;

    private String title;

    private String description;

    @Enumerated(EnumType.STRING)
    private Priority priority;

    @Enumerated(EnumType.STRING)
    private Status status;

    public Task(TaskBuilder taskBuilder) {
        this.id = taskBuilder.id;
        this.title = taskBuilder.title;
        this.description = taskBuilder.description;
        this.dueDate = taskBuilder.dueDate;
        this.createdAt = taskBuilder.createdAt;
        this.updatedAt = taskBuilder.updatedAt;
        this.resolvedAt = taskBuilder.resolvedAt;
        this.priority = taskBuilder.priority;
        this.status = taskBuilder.status;
    }

    public TaskDTO toDTO() {
        return new TaskDTO.TaskDTOBuilder(id, title, description)
                .withPriority(priority)
                .withStatus(status)
                .withDueDate(dueDate)
                .createdAt(createdAt)
                .updatedAt(updatedAt)
                .resolvedAt(resolvedAt)
                .build();
    }

    public static class TaskBuilder {
        public static final String DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy hh:mm:ss";
        private Integer id;

        private Date createdAt;

        private Date updatedAt;

        private Date dueDate;

        private Date resolvedAt;

        private String title;

        private String description;

        @Enumerated(EnumType.STRING)
        private Priority priority;

        @Enumerated(EnumType.STRING)
        private Status status;

        public TaskBuilder(String id, String title) {
            if (id != null) {
                this.id = Integer.parseInt(id);
            }
            this.title = title;
        }

        public TaskBuilder createdAt(String createdAt) {
            DateFormat format = new SimpleDateFormat(DD_MM_YYYY_HH_MM_SS);
            try {
                this.createdAt = format.parse(createdAt);
                return this;
            } catch (NullPointerException | ParseException e) {
                return this;
            }
        }

        public TaskBuilder updatedAt(String updatedAt) {
            DateFormat format = new SimpleDateFormat(DD_MM_YYYY_HH_MM_SS);
            try {
                this.updatedAt = format.parse(updatedAt);
                return this;
            } catch (NullPointerException | ParseException e) {
                return this;
            }
        }

        public TaskBuilder resolvedAt(String resolvedAt) {
            DateFormat format = new SimpleDateFormat(DD_MM_YYYY_HH_MM_SS);
            try {
                this.resolvedAt = format.parse(resolvedAt);
                return this;
            } catch (NullPointerException | ParseException e) {
                return this;
            }
        }

        public TaskBuilder withStatus(String status) {
            this.status = Arrays.stream(Status.values()).filter(s -> s.toString().equals(status)).findFirst().orElse(null);
            return this;
        }

        public TaskBuilder withPriority(String priority) {
            this.priority = Arrays.stream(Priority.values()).filter(p -> p.toString().equals(priority)).findFirst().orElse(null);
            return this;
        }

        public TaskBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public TaskBuilder withDueDate(String dueDate) {
            DateFormat format = new SimpleDateFormat(DD_MM_YYYY_HH_MM_SS);
            try {
                this.dueDate = format.parse(dueDate);
                return this;
            } catch (NullPointerException | ParseException e) {
                return this;
            }
        }

        public Task build() {
            return new Task(this);
        }
    }

}
