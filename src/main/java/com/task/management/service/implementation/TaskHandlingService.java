package com.task.management.service.implementation;

import com.task.management.exception.*;
import com.task.management.model.dto.TaskDTO;
import com.task.management.model.entity.Task;
import com.task.management.repository.TaskRepository;
import com.task.management.service.declaration.TaskHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Scope("singleton")
public class TaskHandlingService implements TaskHandler {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public List<TaskDTO> getAllTasks() {
        return taskRepository.findAllByOrderByCreatedAtAsc().stream().map(Task::toDTO).collect(Collectors.toList());
    }

    @Override
    public Integer createTask(TaskDTO taskDTO) throws NotAllowedPropertyConfigurationException {
        if (taskDTO.getId() != null || taskDTO.getCreatedAt() != null) {
            throw new NotAllowedPropertyConfigurationException();
        }
        Task task = taskDTO.toTask();
        task.setCreatedAt(new Date());
        return taskRepository.save(task).getId();
    }

    @Override
    public void deleteTask(String taskId) throws InvalidTaskIdException {
        try {
            taskRepository.deleteById(Integer.parseInt(taskId));
        } catch (NumberFormatException e) {
            throw new InvalidTaskIdException();
        }
    }

    @Override
    public TaskDTO getTask(String id) throws InvalidTaskIdException, TaskNotFoundException {
        try {
            Integer taskID = Integer.parseInt(id);
            Optional<Task> taskOptional = taskRepository.findById(taskID);
            if (taskOptional.isPresent()) {
                return taskOptional.get().toDTO();
            } else {
                throw new TaskNotFoundException();
            }
        } catch (NumberFormatException e) {
            throw new InvalidTaskIdException();
        }
    }

    @Override
    public TaskDTO updateTask(TaskDTO taskDTO) throws InvalidTaskIdException, PropertyNotNullableException {
        if (taskDTO.getId() == null || taskDTO.getCreatedAt() == null)
            throw new PropertyNotNullableException();
        try {
            Integer.parseInt(taskDTO.getId());
            return taskRepository.save(taskDTO.toTask()).toDTO();
        } catch (NumberFormatException e) {
            throw new InvalidTaskIdException();
        }

    }
}
