package com.task.management.service.implementation;

import com.task.management.exception.InvalidTaskIdException;
import com.task.management.exception.NotAllowedPropertyConfigurationException;
import com.task.management.exception.PropertyNotNullableException;
import com.task.management.exception.TaskNotFoundException;
import com.task.management.model.dto.TaskDTO;
import com.task.management.service.declaration.TaskHandler;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Profile("test")
@Service
public class TaskHandlingMockService implements TaskHandler {
    @Override
    public List<TaskDTO> getAllTasks() {
        return new ArrayList<>();
    }

    @Override
    public Integer createTask(TaskDTO taskDTO) throws NotAllowedPropertyConfigurationException {
        return null;
    }

    @Override
    public void deleteTask(String taskId) throws InvalidTaskIdException {

    }

    @Override
    public TaskDTO getTask(String id) throws InvalidTaskIdException, TaskNotFoundException {
        return null;
    }

    @Override
    public TaskDTO updateTask(TaskDTO taskDTO) throws InvalidTaskIdException, PropertyNotNullableException {
        return null;
    }
}
