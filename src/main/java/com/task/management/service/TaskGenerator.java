package com.task.management.service;

import com.task.management.enumeration.Priority;
import com.task.management.enumeration.Status;
import com.task.management.model.dto.TaskDTO;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Date;
import java.util.Random;

@Service
public class TaskGenerator {

    private static final String[] TITLES = {"Maintenance check", "Call with Richard", "Security analysis", "Daily meeting", "Team building", "Contract discussion"};
    private static final String[] DESCRIPTIONS = {"Mildly important. Consult with CEO", "Not enough info. Discuss with Joanna", "Run a group call to get feedback regarding this subject", "Write this down"};

    public TaskDTO createTask() {
        Random random = new SecureRandom();

        return new TaskDTO.TaskDTOBuilder(getRandomTitle(random))
                .withStatus(Status.CREATED)
                .withDueDate(getRandomDueDate(random))
                .withPriority(getRandomPriority(random))
                .withDescription(getRandomDescription(random))
                .build();
    }


    private String getRandomTitle(Random random) {
        int randomTitleIndex = random.nextInt(TITLES.length);
        return TITLES[randomTitleIndex];
    }

    private String getRandomDescription(Random random) {
        int randomDescriptionIndex = random.nextInt(DESCRIPTIONS.length);
        return DESCRIPTIONS[randomDescriptionIndex];
    }

    private Priority getRandomPriority(Random random) {
        int randomPriorityIndex = random.nextInt(Priority.values().length);
        return Priority.values()[randomPriorityIndex];
    }

    private Date getRandomDueDate(Random random) {
        int randomNumberOfWeeks = random.nextInt(4) + 1;
        return new Date(new Date().getTime() + randomNumberOfWeeks * 7 * (1000 * 60 * 60 * 24));
    }
}
