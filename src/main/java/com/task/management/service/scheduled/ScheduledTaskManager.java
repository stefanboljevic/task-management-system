package com.task.management.service.scheduled;

import com.task.management.exception.NotAllowedPropertyConfigurationException;
import com.task.management.model.dto.TaskDTO;
import com.task.management.service.TaskGenerator;
import com.task.management.service.implementation.TaskHandlingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ScheduledTaskManager {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledTaskManager.class);
    @Autowired
    private TaskHandlingService taskHandlingService;

    @Autowired
    private TaskGenerator taskGenerator;

    @Scheduled(cron = "${task.manager.interval}")
    public void createTask() {
        TaskDTO taskDTO = taskGenerator.createTask();
        try {
            Integer taskID = taskHandlingService.createTask(taskDTO);
            logger.info("Created task with ID: {}, title: {}, description: {} and due date: {}", taskID, taskDTO.getTitle(), taskDTO.getDescription(), taskDTO.getDueDate());
        } catch (NotAllowedPropertyConfigurationException e) {
            logger.error("Error happened while trying to create a task automatically. Message: {}", e.getMessage());
        }
    }

}
