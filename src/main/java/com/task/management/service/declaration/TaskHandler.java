package com.task.management.service.declaration;

import com.task.management.exception.InvalidTaskIdException;
import com.task.management.exception.NotAllowedPropertyConfigurationException;
import com.task.management.exception.PropertyNotNullableException;
import com.task.management.exception.TaskNotFoundException;
import com.task.management.model.dto.TaskDTO;

import java.util.List;

public interface TaskHandler {
    List<TaskDTO> getAllTasks();

    Integer createTask(TaskDTO taskDTO) throws NotAllowedPropertyConfigurationException;

    void deleteTask(String taskId) throws InvalidTaskIdException;

    TaskDTO getTask(String id) throws InvalidTaskIdException, TaskNotFoundException;

    TaskDTO updateTask(TaskDTO taskDTO) throws InvalidTaskIdException, PropertyNotNullableException;
}
