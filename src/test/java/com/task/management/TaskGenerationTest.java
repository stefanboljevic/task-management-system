package com.task.management;

import com.task.management.model.dto.TaskDTO;
import com.task.management.model.entity.Task;
import com.task.management.service.TaskGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TaskGenerationTest {

    @Test
    void testNewlyCreatedTask() {
        TaskGenerator taskGenerator = new TaskGenerator();
        TaskDTO taskDTO = taskGenerator.createTask();
        Task task = taskDTO.toTask();

        Assertions.assertNull(task.getId());
        Assertions.assertNotNull(task.getDescription());
        Assertions.assertNotNull(task.getTitle());
        Assertions.assertNotNull(task.getStatus());
        Assertions.assertNotNull(task.getPriority());
        Assertions.assertNotNull(task.getDueDate());
    }

}
