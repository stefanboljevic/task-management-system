FROM openjdk:11
ADD target/challenge-app.jar challenge-app.jar
ENTRYPOINT ["java", "-jar","challenge-app.jar"]
EXPOSE 8080