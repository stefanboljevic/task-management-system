# Task Management System

TMS API is a Java application that simulates a very
simple task management system, supports both manual and automatic
creation of the tasks.

## Requirements
- Docker (latest)
- Java 11

## Usage
1. Enter the project folder
2. Run the following command to build the .jar file
   - `mvn package -Dmaven.test.skip`
3. Run the following Docker command
   - `docker-compose up -d`
4. Test the app
   1. Since the application has automatic task creation, wait 20 seconds (default) for the first task to be created. Then open your terminal and run the following command:
      1. `curl http://localhost:8080/api/tasks/all`

## Testing with Postman

The application supports CRUD operations. Following operations are available:
- Get all tasks (ordered by creation date)
- Get a single task
- Create a task
- Update a task
- Delete a task

There's a file **_TMS.postman_collection.json_** in the "**resources**" folder, it represents a Postman collection which you can import and use immediately for testing.